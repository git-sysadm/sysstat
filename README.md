# ./SysStat.sh
System Status
--
Creating a system information script extracting the information using coreutils, sed, awk, iproute2 and net-tools package binaries. 


        -----------
        | SysStat |
        -----------

        Display information about System Status.
        Below are the available switches and there usage.

        help    - To view manual of this script.

        ver     - Get version and other info.

        To view specific output use below switches for details.

        short   - Short switch provide summary details of system.
        la      - Load average details minute ago.
        cpu     - Processor details and top 5 CPU consuming process.
        mem     - Physical and swap memory details and current top 5 memory consuming process.
        net     - Network devices and there IP Address.
        pro     - Top 5 memory and CPU consuming process.
        host    - Hostname, running kernel, OS version and current system manager info.
                  For system manager info this command is need to be run with root privilege.
        disk    - Info about the disk and mounted devices.
        h       - Only display hostname.
        all     - To get all info about current system.
        nc      - Monitor Internet connection with 3 second interval.
        nc log  - Redirect output to a file under the path "/tmp/netc/Year-Month-DayoftheMonth.log".
                  To cancel kindly press Ctrl + C combination keys.
                  To keep it running like a daemon then kindly add ampersand (&) at last.

        Example:    ` sysstat.sh  help '
        Run this script without any switch you will get help page.



<br>	

	Running version 0.6.9
	ScriptName: SystemStats
	Author: Sanju P Debnath
	Git Link: https://gitlab.com/linuxcli/sysstat

<br><br>
Sample commands only to run.

`$ bash <(curl -s https://raw.githubusercontent.com/NgineerBabu/SysStat/master/sysstat.sh) ver`    <br>
`$ bash <(curl -s https://raw.githubusercontent.com/NgineerBabu/SysStat/master/sysstat.sh) all`    <br>
`$ bash <(curl -s https://raw.githubusercontent.com/NgineerBabu/SysStat/master/sysstat.sh) short`  <br>

<br><br>
![](help.gif)
